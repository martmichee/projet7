const router = require ('express').Router();

const authController = require('../controllers/posts_controller');

router.post('/',  authController.createPost); 
router.get('/:userid',  authController.getOnePost);
router.get('/',  authController.getAllPosts);
router.put('/:id', authController.modifyPost);
router.delete('/:id',  authController.deletePost );


module.exports = router;