const router = require ('express').Router();

const authController = require('../controllers/comments_controller')

router.post('/',  authController.createComment);
router.get('/',authController.getAllComments); 
router.get('/:userId', authController.getOneComment);
router.delete('/:id', authController.deleteComment );


module.exports = router;
