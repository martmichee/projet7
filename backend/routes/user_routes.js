const router = require('express').Router();

const userController = require('../controllers/user_controller');

router.post("/register",  userController.register);
router.post("/login",  userController.login);



router.get('/', userController.getAllAccount);
router.get('/:id', userController.getOneAccount);
router.put('/:id',  userController.modifyAccount);
router.delete('/:id', userController.deleteAccount);

module.exports = router;