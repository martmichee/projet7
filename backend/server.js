const express = require("express");
const userRoutes = require('./routes/user_routes');
const postsRoutes = require('./routes/post_routes');
const commentsRoutes = require('./routes/comment_routes')
// const bodyParser = require('body-parser');
// const cookieParser = require('cookie-parser');
const cookiesMiddleware = require('universal-cookie-express');
const cors = require('cors');
const {checkUser, requireAuth} = require('./middleware/auth_middleware');
require('dotenv').config({path: './config/env'});
require('./connectBd');

const app = express();
require('dotenv').config({
  path: './config/.env'
});
const helmet = require('helmet');

// app.use((req, res, next) => {
//   res.setHeader('Access-Control-Allow-Origin', '*');
//   res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content, Accept, Content-Type, Authorization');
//   res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
//   next();
// });
const corsOptions = {
  origin: process.env.CLIENT_URL,
  credentials: true,
  'allowedHeaders': ['sessionId', 'Content-Type'],
  'exposedHeaders': ['sessionId'],
  'methods': 'GET,HEAD,PUT,PATCH,POST,DELETE',
  'preflightContinue': false
}

app.use(cors(corsOptions));
// app.use(cookieParser());
app.use(express.json());
app.use(express.urlencoded({
  extended: true
}));


// app.get('*', checkUser);
  app.use(cookiesMiddleware())
  app.get('/jwtid', requireAuth, (req, res) => {
  // req.universalCookies.get(token)
  res.status(200).send(token)
});


// routes
app.use('/api/user', userRoutes);
app.use('/api/posts', postsRoutes);
app.use('/api/comments', commentsRoutes);
app.use(helmet());



// server
app.listen(process.env.PORT, () => {
  console.log(`listening on ${process.env.PORT}`);
});