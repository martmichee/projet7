//const DataTypes = sequelize.DataTypes;
const User = require('../models/user_model');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const passwordValidator = require('password-validator');
const mailValidator = require('email-validator');



module.exports = {

    // S'enregistrer
    register: async function (req, res) {
        const schema = new passwordValidator();
        schema
            .is().min(4)
            .is().max(100)
            .has().uppercase()
            .has().lowercase()
            .has().digits(2)
            .has().not().spaces()
            .is().not().oneOf(['Passw0rd', 'Password123']);
        const salt = bcrypt.genSaltSync(10);
        const hash = bcrypt.hashSync(req.body.password, salt);
        const user = {
            firstName: req.body.firstName,
            lastName: req.body.lastName,
            email: req.body.email,
            password: hash
        }
        if (!mailValidator.validate(req.body.email) || (!schema.validate(req.body.password))) {
            return res.status(404).json({
                error: " invalide !"
            })
        }
        if (user.firstName == null || user.lastName == null || user.email == null || user.password == null) {
            return res.status(400).json({
                'error': 'champs obligatoire'
            })
        }
        
        try {
            const newUser = await User.create(user);
           

            return res.status(201).json({
                'message': 'utilisateur créé'
            })
        } catch (error) {
            res.status(500).json({
                error
            })
        }
    },

    // s'identifier   
    login: async function (req, res, next) {
        User.findOne({
                where: {
                    email: req.body.email,
                }
            })
            .then(user => {
                if (!user) {
                    return res.status(401).json({
                        error: 'Utilisateur inconnu !'
                    });
                }               
                bcrypt.compare(req.body.password, user.password)
                    .then(valid => {
                        if (!valid) {
                            return res.status(401).json({
                                error: 'Mot de passe incorrect !'
                            });
                        }
                        const maxAge = 1 * 24 * 60 * 60;
                        const token = jwt.sign({
                                userId: user.id,
                                isAdmin: user.isAdmin                                
                            },
                            'RANDOM_TOKEN_SECRET', {
                                expiresIn: maxAge
                            }
                        );                        
                        res.cookie('jwt', token, {
                            httpOnly: true,
                            maxAge
                        });
                        res.status(200).json({token});
                    })
                    .catch(error => res.status(501).json({
                        error
                    }));
            })

            .catch(error => res.status(503).json({
                error
            }));
    },
   
    // se supprimer
    deleteAccount: async function (req, res, next) {
        User.findOne({
                where: {
                    id: req.params.id
                }
            })
            .then(() => {
                User.destroy({
                        where: {
                            id: req.params.id
                        }
                    })
                    .then(() => res.status(200).json({
                        message: 'compte supprimé'
                    }))
                    .catch(error => res.status(400).json({
                        error
                    }));
            })
            .catch(error => res.status(500).json({
                error
            }));
    },

    // modifier un compte
    modifyAccount: async function (req, res, next) {
        User.findOne({
                where: {
                    id: req.params.id
                }
            })
            .then((user) => {
                lastname = req.body.lastname;
                firstname = req.body.firstname;
                email = req.body.email;
                User.update()
                    .then(() => res.status(201).json({
                        message: 'Compte modifié !'
                    }))
                    .catch(error => res.status(400).json({
                        error
                    }));
            })
            .catch(error => res.status(500).json({
                error
            }));
    },

    // trouver un compte
    getOneAccount: async function (req, res, next) {
        User.findOne({
                where: {
                    id: req.params.id
                }
            })
            .then((user) => res.status(200).json(user))
            .catch(error => res.status(404).json({
                error: error.message
            }))
    },

    // trouver tous les comptes
    getAllAccount: async function (req, res, next) {
        User.findAll()
            .then((users) => res.status(200).json(users))
            .catch(error => res.satuts(404).json({
                error: error.message
            }))
    }

}