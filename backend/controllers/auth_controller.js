const jwt = require('jsonwebtoken');
const User = require('../models/user_model');
const bcrypt = require('bcrypt');

module.exports= {
  login: async function (req, res, next) {

  User.findOne({
          where: {
              email: req.body.email,
          }
      })
      .then(user => {
          if (!user) {
              return res.status(401).json({
                  error: 'Utilisateur inconnu !'
              });
          }
          console.log(user);
          bcrypt.compare(req.body.password, user.password)
              .then(valid => {
                  if (!valid) {
                      return res.status(401).json({
                          error: 'Mot de passe incorrect !'
                      });
                  }
        

                  const maxAge = 1 * 24 * 60 * 60;
                  const token = jwt.sign({
                          userId: user.id,
                          userName: user.lastName,
                      },
                      'RANDOM_TOKEN_SECRET', {
                          expiresIn: maxAge
                      }, {
                          isAdmin: user.isAdmin
                      }
                  );
                  res.cookie('jwt', token, {
                      httpOnly: true,
                      maxAge
                  });
                  console.log(process.env.REACT_APP_API_URL);
                  res.status(200).json('connected');
                  
              })
               
              .catch(error => res.status(501).json({
                  error
              }));
      })

      .catch(error => res.status(503).json({
          error
      }));
}}