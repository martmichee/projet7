module.exports = ownerId => {
    const token = req.headers.authorization.split(' ')[1];        
    const decodedToken = jwt.verify(token, 'RANDOM_TOKEN_SECRET');        
    const userId = decodedToken.userId; 
    if (userId !== ownerId) {
        return res.status(403).json({message : 'action non autorisée'})
    }
}
