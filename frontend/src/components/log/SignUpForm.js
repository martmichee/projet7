import React, { useState } from 'react';
import axios from 'axios';
import SignInForm from './SignInForm';


const SignUpForm =  () => {
    const [formSubmit, setFormSumit] = useState(false);
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [showPassword, setShowPassword] = useState(false);

    const handleRegister = async (e) => {
        e.preventDefault();
        const terms = document.getElementById('terms');
        const firstNameError = document.querySelector('.firstName.error');
        const lastNameError = document.querySelector('.lastName.error');
        const emailError = document.querySelector('.email.error');
        const passwordError = document.querySelector('.password.error');
        const termsError = document.querySelector('.terms.error');
        termsError.innerHTML = "";
        
        if (!terms.checked){
            termsError.innerHTML = "Veuillez valider les conditions générales.";
        }else{
         await axios({
             method: "post",
             url: `${process.env.REACT_APP_API_URL}api/user/register`,
             data:{
            firstName,
             lastName,
             email,
             password
            }
         })
         .then((res) => {
             console.log(res);
             if (res.data.errors){
                 firstNameError.innerHTML = res.data.errors.firstName;
                 lastNameError.innerHTML = res.data.errors.lastName;
                 emailError.innerHTML = res.data.errors.email;
                 passwordError.innerHTML = res.data.errors.password;
             } else {
                 setFormSumit(true);
             }            
         })  
         .catch((err) => console.log(err)) 
        }
}
    
        return (
            <>
            {formSubmit ? (
                <>
               <SignInForm/>
               <span></span>
               <h4 className='success'>Enregistrement réussi, veuillez-vous connecter.</h4>
               </>
            ) : (
        <form action="" onSubmit={handleRegister} id="sign-up-form">
            <label htmlFor="name">Nom</label>
          <br />
          <input type="text" name='name' id='firstName' onChange={(e) => setFirstName(e.target.value)} value={firstName} />
          <div className="firstName error"></div>
            <br />
            <label htmlFor="name">Prénom</label>
          <br />
          <input type="text" name='name' id='lastName' onChange={(e) => setLastName(e.target.value)} value={lastName} />
          <div className="lastName error"></div>
            <br />
            <label htmlFor="email">Email</label>
          <br />
          <input type="email" name='mail' id='email' onChange={(e) => setEmail(e.target.value)} value={email} />
          <div className="email error"></div>
            <br />
            <label htmlFor="name">Mot de passe</label>
          <br />
          <input type={showPassword ? 'text' : 'password'} name='password' id='password' onChange={(e) => setPassword(e.target.value)} value={password} />
          <br />
          <button type="button"  className='active-btn' onClick={() => setShowPassword(!showPassword)}>{showPassword ? 'Cacher' : 'Afficher'}</button>
          <div className="password error"></div>
            <br />
            <input type='checkbox' id='terms'/>
            <label htmlFor='terms'> J'accepte les <a href='/' target='_blank' rel='noopener noreferrer'> conditions générales</a></label>
            <div className="terms error"></div>
            <br />
 
            <input type='submit' value='Valider votre inscription'/>
        </form>
        ) }
        </>
    );
};

export default SignUpForm