import React from 'react';
import { NavLink } from 'react-router-dom';

const LeftNav = () => {
    return (
        <div className="left-nav-container">
            <div className="icons">
                <div className="icons-bis">
                    <NavLink to='/' exact activeClassName='active-left-nav'>
                        <img src="./asset/img/home.svg" alt="" />
                    </NavLink>
                    <br />
                    <NavLink to='/trending' exact activeClassName='active-left-nav'>
                        <img src="./asset/img/rocket.svg" alt="" />
                    </NavLink>
                    <br />
                    <NavLink to='/profil' exact activeClassName='active-left-nav'>
                        <img src="./asset/img/user.svg" alt="" />
                    </NavLink>
                </div>
            </div>
        </div>
    );
};

export default LeftNav;