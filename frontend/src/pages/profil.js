import React, { useContext } from 'react';
import Log from '../components/log'
import { UidContext } from '../components/AppContext';
import UpdateProfil from '../components/profil/UpdateProfil';
// import LeftNav from "../components/LeftNav";
const Profil = () => {
    const uid = useContext(UidContext);
    return (        
        <div className="profil-page">
            {uid ? (
                <UpdateProfil/>
            ) : (
            <div className="log-container">
<Log signin={false} signup={true}/>
<div className="img-container">
    <img src="asset/img/inscription.jpg" alt="dessin d'une femme qui est assise et qui s'incrit à un site. derrière elle, un écran avec dessus un formulaire d authentification" />
</div>
            </div>
            )}
        </div>
    );
};

export default Profil;