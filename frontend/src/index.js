import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import App from './App';
import './styles/index.scss';
import {applyMiddleware, createStore} from 'redux';
import thunk from 'redux-thunk';
import rootReducer from './reducers'
import { getPosts } from "./actions/post_actions";

// dev Tools
import logger from 'redux-logger';
import {composeWithDevTools} from 'redux-devtools-extension';


const store = createStore(
rootReducer, composeWithDevTools(applyMiddleware(thunk, logger))
)

store.dispatch(getPosts());

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById("root")
);


